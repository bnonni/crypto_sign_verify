# Encryptor | Securing All Your Cryptography Needs
## Know Before You Go
* Core:
      * `Node.js v12.12.0`
      * `npm v6.11.3`
      * `MongoDB shell and db v4.2.1`
* Dependencies:
      * `bcryptjs: ^2.4.3`
      * `crypto: ^1.0.1`
      * `debug: ~2.6.9`
      * `ejs: ~2.6.1`
      * `ejs-lint: ^0.3.0`
      * `express: ~4.16.1`
      * `express-session: ^1.17.0`
      * `http-errors: ~1.6.3`
      * `jsonwebtoken: ^8.5.1`
      * `mongoose: ^5.7.9`
      * `morgan: ~1.9.1`
      * `nodemon: ^1.19.4`

## What's what?
* `app/`
      * `|-bin/www` Server start file
      * `|-config/keys` mongoURI to connect mongoose to MongoDB
      * `|-config/db` Mongoose connection to MongoDB setup
      * `|-models/User` Mongoose User schema for adding new MongoDB documents
      * `|-public/javascripts/validation.js` Static, client-side JS to validate username & password
      * `|-public/stylesheets/style.css` Static CSS sheet
      * `|-routes/index` Main router file containing all app routes
      * `|-tests/` Test scripts written in isolation for crypto functionality
      * `|-views/` All ejs render templates for viewing engine
      * `|-app.js` App creation file

## Now You Know, Time To Go
1. Clone this repo
   SSH
   ```
   git clone git@github.com:bnonni/BitPay.git
   ```
   HTTPS
   ```
   git clone https://github.com/bnonni/BitPay.git
   ```

2. Open the repo in an editor, and `cd` into the app folder. 
   ```
   cd app
   ```

3. Inside app/ folder, install dependencies:
   ```
   npm install
   ```

4. Start your localhost mongoDB server (many ways to do this)
   ```
   mongod
   ```

5. Start the app from the app/ folder 
   ```
   npm run app
   ```

6. Nagivate to [127.0.0.1:3000](http://127.0.0.1:3000)

## Features & Usability
* Home Page: [127.0.0.1:3000/](http://127.0.0.1:3000/)
      * Set Username, Set Password: user can set a user name and password
      * Verify Message: anyone can visit the /verify page from home page
* Users Page: [127.0.0.1:3000/users](http://127.0.0.1:3000/users)
      * Page inaccessible publicly; must set username & password first
      * Generate Key Pair: authenticated user can generate a new public/ private RSA key pair to use for signing and verifying
      * Store Public Key: Allows an authenticated user to store a public key on the server
      * Sign Message: Signs a message using a private key
* Verify Page: [127.0.0.1:3000/verify](http://127.0.0.1:3000/verify)
      * Publicly accessable page
      * Verify Signed Message: Allows anyone to submit a signed message to the server to verify if it has been signed by the private key associated with a specified user�s public key




